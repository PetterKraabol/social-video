<!DOCTYPE>
<html>
	<head>
		<title>Video</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="/template/css/style.css"/>
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	</head>
	<body>
		<div id="profile-dropdown" class="shadow">
			<div class="dropdown-window-top"></div>
			<div class="profile-summary">
				<img class="circular" src="/img/avatar.png" width="64">
				<br>
				<span class="email">petter.zarlach@gmail.com</span>
				<br>
				<div class="toggle-slide"></div>
			</div>
			<a href="#" class="logout"><span>Log Out</span></a>
		</div>
		<div id="top">
			<div id="logo">
				<h1>Video</h1>
			</div>
			<div id="search">
				<input type="text" placeholder="Search">
			</div>
			<a href="#" class="top-button upload">
				<span>Upload</span>
			</a>
			<a href="#" class="top-button profile">
				<span>Petter Kraabøl<img height="16" width="16" src="/template/img/dropdown-arrow-gray.png"></span>
			</a>
		</div>
		<div id="sidebar">
			<div id="guide">
				<h4>Guide</h4>
				<ul>
					<a href=""><li>Petter Kraabøl</li></a>
					<a href=""><li>Subscriptions</li></a>
					<a href=""><li>Explore</li></a>
					<a href=""><li>Social</li></a>
					<a href=""><li>Trending</li></a>
					<a href=""><li>History</li></a>
				</ul>
			</div>
		</div>
		<div id="main-content">
			<div class="video-box">
				<a href="#" class="video-overlay"><img src="/template/img/play-overlay.png" height="150"></a>
				<img src="https://secure-b.vimeocdn.com/ts/459/003/459003792_640.jpg" width="320" height="180">
				<h3>A sense of place</h3>
			</div>
			<div class="video-box">
				<img src="https://secure-b.vimeocdn.com/ts/456/236/456236741_640.jpg" width="320" height="180">
			</div>
			<div class="video-box">
				<img src="https://secure-b.vimeocdn.com/ts/458/926/458926418_640.jpg" width="320" height="180">
			</div>
			<div class="video-box">
				<img src="https://secure-b.vimeocdn.com/ts/458/984/458984315_640.jpg" width="320" height="180">
			</div>
			<div class="video-box">
				<img src="https://secure-b.vimeocdn.com/ts/458/793/458793627_640.jpg" width="320" height="180">
			</div>
			<div class="video-box">
				<img src="https://secure-b.vimeocdn.com/ts/458/206/458206358_640.jpg" width="320" height="180">
			</div>
			<div class="video-box">
				<img src="https://secure-b.vimeocdn.com/ts/458/215/458215394_640.jpg" width="320" height="180">
			</div>
			<div class="video-box">
				<img src="https://secure-b.vimeocdn.com/ts/458/354/458354096_640.jpg" width="320" height="180">
			</div>
			<div class="video-box">
				<img src="https://secure-b.vimeocdn.com/ts/458/186/458186682_640.jpg" width="320" height="180">
			</div>
			<div class="video-box">
				<img src="https://secure-b.vimeocdn.com/ts/458/662/458662830_640.jpg" width="320" height="180">
			</div>
			<div class="video-box">
				<img src="https://secure-b.vimeocdn.com/ts/457/807/457807989_640.jpg" width="320" height="180">
			</div>
			<div class="video-box">
				<img src="https://secure-b.vimeocdn.com/ts/458/378/458378345_640.jpg" width="320" height="180">
			</div>
			<div class="clear-both"></div>
		</div>
		<div id="footer"></div>
		<script src="/template/js/script.js"></script>
	</body>
</html>